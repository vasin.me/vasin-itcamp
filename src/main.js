// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue';
import Vuex from 'vuex';
import App from './App';
import router from './router';
import Raven from 'raven-js';
import RavenVue from 'raven-js/plugins/vue';
import Bootstrap from 'bootstrap';
import VeeValidate from 'vee-validate';
import store from '@store/main';
import jQuery from 'jquery';
import {
  TweenMax,
  Power2,
  TimelineLite
} from 'gsap';
import ScrollToPlugin from 'gsap/ScrollToPlugin';
import fontawesome from '@fortawesome/fontawesome';
import {
  caretLeft,
  caretRight,
  faPowerOff
} from '@fortawesome/fontawesome-free-solid';
import {
  faFacebookF
} from '@fortawesome/fontawesome-free-brands';
// import VueResource from 'vue-resource';

import 'gsap';
import 'gsap/jquery.gsap';
import '@scss/layouts/app.scss';
import '../node_modules/bootstrap/scss/bootstrap.scss';
// import keys from '../../config/keys';

window.jQuery = jQuery;
// Vue.use(VueResource);
Vue.use(VeeValidate);
Vue.use(Vuex);
Vue.use(VeeValidate, {
  inject: false
});
// Raven.config('https://afe6d8070c264076bbb500d42281fc43@sentry.itcamp.in.th/3')
//   .addPlugin(RavenVue, Vue)
//   .install()

Vue.config.productionTip = false;
new Vue({
  el: '#app',
  store,
  router,
  components: {
    App
  },
  template: '<App/>'
});