import Vue from 'vue';
import Vuex from 'vuex';
import VueResource from 'vue-resource';
import router from '../router';
import Swal from 'sweetalert2';
import axios from 'axios';
import swal from 'sweetalert2';
import {
  resolve
} from 'upath';

Vue.use(Vuex);

const state = {
  forcePage: 'Reg_Subcamp_Game',
  csrf: '',
  isLoading: false,
  isFormLoading: false,
  env: {
    host: location.protocol + '//' + location.host,
    path: location.pathname
  },
  user: {
    login: false
  },
  form: {
    personal: [],
    camp: [],
    subcamp: []
  },
  submit: {},
  userProgress: {}
};
const mutations = {
  forceCamp(state, name) {
    if (name == 'GAME ORION') {
      state.forcePage = 'Reg_Subcamp_Game';
    } else if (name == 'I-OPOLLO') {
      state.forcePage = 'Reg_Subcamp_Iot';
    } else if (name == 'NETWORK STELLAR') {
      state.forcePage = 'Reg_Subcamp_Network';
    } else if (name == 'APPLIDANUS') {
      state.forcePage = 'Reg_Subcamp_App';
    } else if (name == 'DTA48') {
      state.forcePage = 'Reg_Subcamp_Data';
    }
  },
  updateForceCamp(state, name) {
    if (name == 'GAME ORION') {
      state.forcePage = 'Reg_Subcamp_Game';
    } else if (name == 'I-OPOLLO') {
      state.forcePage = 'Reg_Subcamp_Iot';
    } else if (name == 'NETWORK STELLAR') {
      state.forcePage = 'Reg_Subcamp_Network';
    } else if (name == 'APPLIDANUS') {
      state.forcePage = 'Reg_Subcamp_App';
    } else if (name == 'DTA48') {
      state.forcePage = 'Reg_Subcamp_Data';
    }
  },
  updateUserProgress(state, payload) {
    state.userProgress = payload;
  },
  updateUserState(state, payload) {
    state.user = payload;
  },
  updateLoadingState(state, payload) {
    if (state.isLoading) {
      setTimeout(function () {
        state.isLoading = payload;
      }, 1500);
    } else {
      state.isLoading = payload;
    }
  },
  updateFormLoadingState(state, payload) {
    if (state.isFormLoading) {
      setTimeout(function () {
        state.isFormLoading = payload;
      }, 1500);
    } else {
      state.isFormLoading = payload;
    }
  },
  updateFormData(state, payload) {
    state.form = payload;
  },
  updateCSRF(state, payload) {
    state.csrf = payload;
  },
  saveData(state, payload) {
    let pagenum = mutations.formNum();
    if (pagenum) state.submit[pagenum] = payload;
  },
  updateValidator(state, payload) {
    state.validator = payload;
  },
  formNum() {
    let pathname = location.pathname.split('/')[location.pathname.split('/').length - 1];
    var pagenum;
    switch (pathname) {
      case 'personal':
        pagenum = 1;
        break;
      case 'camp':
        pagenum = 2;
        break;
    }
    if (location.pathname.split('/').indexOf('subcamp') > 0) {
      pagenum = 3;
    }
    return pagenum;
  }
};

const getters = {
  getLoadingState: (state) => state.isLoading,
  getFormLoadingState: (state) => state.isFormLoading,
  getUserLogin: (state) => state.user.login,
  getUserInfo: (state) => state.user.info,
  getFormData: (state) => state.form,
  getCSRF: (state) => state.csrf,
  getValidator: (state) => state.validator,
  getUserProgress: (state) => state.userProgress,
  getForcePage: (state) => state.forcePage
};

const actions = {
  getAnnouncement() {
    return new Promise((resolve, reject) => {
      axios
        .get(state.env.host + '/14/api/announcement')
        .then((response) => {
          return resolve(response);
        })
        .catch((err) => {
          return reject(err.response);
        });
    })
  },
  rejectConfirm({
    commit
  }, data) {
    return new Promise((resolve, reject) => {
      // get csrf
      function confirmCSRF() {
        return new Promise((resolve, reject) => {
          axios
            .get(state.env.host + '/14/api/csrf')
            .then((csrfData) => {
              resolve(csrfData);
            })
            .catch((err) => {
              reject(err.response);
            });
        });
      }

      // get user
      function confirmUser() {
        return new Promise((resolve, reject) => {
          axios
            .get(state.env.host + '/14/api/auth/current')
            .then((user) => {
              if (user.data.login) {
                resolve(user);
              } else {
                reject('user not login.');
              }
            })
            .catch((err) => {
              reject(err.response);
            });
        });
      }

      // exe
      confirmCSRF().then((csrf) => {
        confirmUser().then((user) => {
          // create axios config header
          let config = {
            headers: {
              'csrf-token': csrf.data.csrfToken,
              'Content-Type': 'multipart/form-data'
            },
            timeout: 0
          };

          // create payload
          let payload = {
            uid: user.data.info.uid
          };

          axios
            .post(state.env.host + '/14/api/register/reject', payload, config)
            .then((response) => {
              state.userProgress.status = 'CANCEL';
              return resolve(response);
            })
            .catch((err) => {
              return reject(err.response);
            });
        });
      });
    });
  },

  submitConfirm({
    commit
  }, data) {
    return new Promise((resolve, reject) => {
      // get csrf
      function confirmCSRF() {
        return new Promise((resolve, reject) => {
          axios
            .get(state.env.host + '/14/api/csrf')
            .then((csrfData) => {
              resolve(csrfData);
            })
            .catch((err) => {
              reject(err.response);
            });
        });
      }

      function confirmUser() {
        return new Promise((resolve, reject) => {
          axios
            .get(state.env.host + '/14/api/auth/current')
            .then((user) => {
              if (user.data.login) {
                resolve(user);
              } else {
                reject('user not login.');
              }
            })
            .catch((err) => {
              reject(err.response);
            });
        });
      }

      confirmCSRF().then((csrf) => {
        confirmUser().then((user) => {
          // create axios config header
          let config = {
            headers: {
              'csrf-token': csrf.data.csrfToken,
              'Content-Type': 'multipart/form-data'
            },
            timeout: 0
          };

          var payload = new FormData();
          for (var key in data) {
            payload.append(key, data[key]);
          }

          axios
            .post(state.env.host + '/14/api/register/confirm', payload, config)
            .then((response) => {
              state.userProgress.status = 'CONFIRM';
              return resolve(response);
            })
            .catch((err) => {
              return reject(err.response);
            });
        });
      });
    });
  },

  checkConfirm({
    commit
  }) {
    return new Promise((resolve, reject) => {
      // get csrf
      function confirmCSRF() {
        return new Promise((resolve, reject) => {
          axios
            .get(state.env.host + '/14/api/csrf')
            .then((csrfData) => {
              resolve(csrfData);
            })
            .catch((err) => {
              reject(err.response);
            });
        });
      }

      function confirmUser() {
        return new Promise((resolve, reject) => {
          axios
            .get(state.env.host + '/14/api/auth/current')
            .then((user) => {
              if (user.data.login) {
                resolve(user);
              } else {
                reject('user not login.');
              }
            })
            .catch((err) => {
              reject(err.response);
            });
        });
      }

      confirmCSRF().then((csrf) => {
        confirmUser().then((user) => {
          // create axios config header
          let config = {
            headers: {
              'csrf-token': csrf.data.csrfToken,
              'Content-Type': 'multipart/form-data'
            },
            timeout: 0
          };

          // create payload
          let payload = {
            uid: user.data.info.uid
          };

          axios
            .get(state.env.host + '/14/api/register/confirm', payload, config)
            .then((response) => {
              return resolve(response);
            })
            .catch((err) => {
              return reject(err.response);
            });
        });
      });
    });
  },
  // requestLogin({
  //   commit
  // }) {
  //   // Enable loading and call reuqest to auth
  //   commit('updateLoadingState', true);
  //   axios
  //     .get(state.env.host + '/14/api/auth/current')
  //     .then((response) => {
  //       // request user state
  //       if (response.status == 200) {
  //         // update user state
  //         commit('updateUserState', response.data);
  //         actions.activeUserProgress;
  //         // disable loading
  //         commit('updateLoadingState', false);
  //       } else {
  //         // Redirect to index and throw error
  //         router.push({
  //           name: 'Index'
  //         });
  //         commit('updateLoadingState', false);
  //         commit('updateUserState', {
  //           login: false
  //         });
  //         alertError(response.status);
  //       }
  //     })
  //     .catch((errors) => {
  //       // Redirect to index and throw error
  //       router.push({
  //         name: 'Index'
  //       });
  //       commit('updateLoadingState', false);
  //       commit('updateUserState', {
  //         login: false
  //       });
  //       alertError(errors.response);
  //     });
  // },
  activeUserProgress({
    commit
  }) {
    axios
      .get(state.env.host + '/14/api/register/current')
      .then((response) => {
        if (response.status == 200) {
          commit('updateUserProgress', response.data);
        } else {
          alertError(response);
        }
      })
      .catch((errors) => {
        alertError(errors.response);
      });
  },
  async requestForm({
    commit
  }, page) {
    // request Form
    commit('updateFormLoadingState', true);
    await axios.get(state.env.host + '/14/api/register/' + page.camp + '/' + page.page).then((form) => {
      if (form.status == 200 && form.data.code != 404) {
        var data = form.data.sort(sortObject);
        var subcamp = new Array();
        var camp = new Array();
        var personal = new Array();

        for (let key in data) {
          let items = data[key];
          if (items.priority / 100 >= 3) {
            subcamp.push(items);
          } else if (items.priority / 100 >= 2 && items.priority / 100 < 3) {
            camp.push(items);
          } else if (items.priority / 100 >= 1 && items.priority / 100 < 2) {
            personal.push(items);
          }
        }
        data = {
          personal,
          camp,
          subcamp
        };
        commit('updateFormData', data);
      } else {
        alertError(form);
        commit('updateFormLoadingState', false);
      }
    });
  },

  loginActive({
    commit
  }) {
    // loading & update state to false
    commit('updateLoadingState', true);
    commit('updateUserState', {
      login: false
    });
    window.location.replace('/14/api/auth/facebook');
  },

  async saveForm({
    commit,
    getters
  }, page) {
    // get csrf
    // commit('updateLoadingState', true);
    await axios.get(state.env.host + '/14/api/csrf').then((csrfData) => {
      commit('updateCSRF', csrfData.data);
    });
    let csrf = getters.getCSRF;

    // create axios config
    let config = {
      headers: {
        'csrf-token': csrf.csrfToken,
        'Content-Type': 'multipart/form-data'
      },
      timeout: 0
    };

    // set data to send
    var payload = new FormData();
    var data = state.submit[page.page];
    for (var key in data) {
      payload.append(key, data[key]);
    }
    if (payload) {
      // request to save data
      await axios
        .post('/14/api/register/' + page.camp.toUpperCase() + '/' + page.page, payload, config)
        .then((response) => {
          if (response.status == 200) {
            // Submit form
            if (page.camp != 'GLOBAL') {
              var subpage = '';
              if (page.camp == 'game') {
                subpage = 'GAMEORION';
              } else if (page.camp == 'iot') {
                subpage = 'I-OPOLLO';
              } else if (page.camp == 'network') {
                subpage = 'NETWORK STELLAR';
              } else if (page.camp == 'app') {
                subpage = 'APPLIDANUS';
              } else if (page.camp == 'data') {
                subpage = 'DTA48';
              }
              if (!page.submit) {
                swal({
                  title: 'น้องต้องการสมัครค่าย ' + subpage + ' หรือไม่?',
                  text: 'เมื่อส่งฟอร์มนี้แล้วจะไม่สามารถแก้ไขได้ น้องต้องการส่งฟอร์มหรือไม่?',
                  type: 'warning',
                  showCancelButton: true,
                  focusConfirm: false,
                  cancelButtonText: 'ไม่, กรอกฟอร์มต่อ, เลือกค่ายใหม่',
                  confirmButtonText: 'ใช่, ยืนยันการส่งฟอร์ม'
                }).then((result) => {
                  if (result.value) {
                    axios
                      .post('/14/api/register/submit/' + page.camp.toUpperCase(), payload, config)
                      .then((response) => {
                        if (response.status == 200 && response.data) {
                          Swal('Success', 'ทำการสมัครเสร็จสิ้น', 'success');
                          router.push({
                            name: 'Reg_Summary'
                          });
                        } else {
                          router.push({
                            name: 'Reg_Auth'
                          });
                          alertError(response.status);
                        }
                      })
                      .catch((errors) => {
                        alertError(errors.response.status);
                      });
                  }
                });
              } else {
                router.push({
                  name: page.to
                });
              }
            } else {
              router.push({
                name: page.to
              });
            }
          } else {
            router.push({
              name: 'Reg_Auth'
            });
            alertError(response.status);
          }
          commit('updateLoadingState', false);
        })
        .catch((error) => {
          router.go(0);
          commit('updateLoadingState', false);
          alertError(error.response);
        });
    } else {
      commit('updateLoadingState', false);
    }
  }
};

function alertError(code) {
  if (code) {
    switch (code.data.error) {
      case 'Field input is missing':
        Swal('Warning', 'ไม่พบฟิลด์ ' + code.data.field + '.', 'warning');
        throw 'field ' + code.data.field + ' is missing.';
        break;
      case 'RegExp error or missing!':
        Swal('Warning', 'รูปแบบไม่ถูกต้องที่ฟิลด์' + code.data.field + '.', 'warning');
        throw 'field ' + code.data.field + ' wrong regex.';
    }
    switch (code.status) {
      case 504:
        Swal(
          'Something went wrong',
          'ไม่สามารถเชื่อมต่อกับเซิฟเวอร์ได้ กรุณาลองใหม่ในภายหลัง ' + code.status + '.',
          'error'
        );
        throw 'gateway timeout, code ' + code.status + '.';
        break;
      case 401:
        throw 'you are not logged in, code ' + code.status + '.';
        break;
      case 400:
        Swal('Something went wrong', 'ไม่สามารถทำตามที่ร้องขอได้ กรุณาลองใหม่อีกครั้ง ' + code.status + '.', 'error');
        throw 'bad request ' + code.status + '.';
      default:
        Swal('Something went wrong', 'กรุณาติดต่อผู้ดูแลระบบ ' + code.status + '.', 'error');
        throw 'throw error, code ' + code.status + '.';
        break;
    }
  }
}

function sortObject(a, b) {
  if (a.priority < b.priority) return -1;
  if (a.priority > b.priority) return 1;
  return 0;
}

let store = new Vuex.Store({
  state: state,
  mutations: mutations,
  getters: getters,
  actions: actions
});

export default store;